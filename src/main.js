var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var title = [
  ['a', 'g', 'm', 's', 'y', '4'],
  ['b', 'h', 'n', 't', 'z', '5'],
  ['c', 'i', 'o', 'u', '0', '6'],
  ['d', 'j', 'p', 'v', '1', '7'],
  ['e', 'k', 'q', 'w', '2', '8'],
  ['f', 'l', 'r', 'x', '3', '9']
]
var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < title.length; i++) {
    for (var j = 0; j < title[i].length; j++) {
      noFill()
      stroke(255)
      strokeWeight(boardSize * 0.03 * abs(sin(frameCount * 0.025 + i * 0.25 + j * 0.25 + i * j)))
      strokeCap(ROUND)
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(title.length * 0.5) + 0.5) * boardSize * 0.135, windowHeight * 0.5 + (j - Math.floor(title[i].length * 0.5) + 0.5) * boardSize * 0.135)
      drawLetter(title[i][j], boardSize * 0.1)
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawLetter(char, s) {
  if (char.toLowerCase() === 'a') {
    line(-0.5 * s, 0.5 * s, -0.5 * s, 0)
    line(0.5 * s, 0.5 * s, 0.5 * s, 0)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(-0.5 * s, 0, 0, -0.5 * s)
    line(0.5 * s, 0, 0, -0.5 * s)
  }
  if (char.toLowerCase() === 'b') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0, 0)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(0, 0.5 * s, 0.5 * s, 0)
    line(-0.5 * s, 0.5 * s, 0, 0.5 * s)
  }
  if (char.toLowerCase() === 'c') {
    line(0.5 * s, -0.5 * s, 0, -0.5 * s)
    line(-0.5 * s, 0, 0, -0.5 * s)
    line(-0.5 * s, 0, 0, 0.5 * s)
    line(0.5 * s, 0.5 * s, 0, 0.5 * s)
  }
  if (char.toLowerCase() === 'd') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(-0.5 * s, 0.5 * s, 0, 0.5 * s)
    line(0.5 * s, 0, 0, 0.5 * s)
    line(0, -0.5 * s, -0.5 * s, -0.5 * s)
    line(0, -0.5 * s, 0.5 * s, 0)
  }
  if (char.toLowerCase() === 'e') {
    line(-0.5 * s, 0, 0, 0)
    line(-0.5 * s, 0, -0.5 * s, -0.5 * s)
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0, -0.5 * s, 0.5 * s)
    line(0.5 * s, 0.5 * s, -0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === 'f') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0, 0)
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
  }
  if (char.toLowerCase() === 'g') {
    line(-0.5 * s, 0, -0.5 * s, 0.5 * s)
    line(0.5 * s, 0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, 0.5 * s, 0.5 * s, 0)
    line(0.5 * s, 0, 0, 0)
    line(-0.5 * s, 0, 0, -0.5 * s)
    line(0.5 * s, -0.5 * s, 0, -0.5 * s)
  }
  if (char.toLowerCase() === 'h') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
  }
  if (char.toLowerCase() === 'i') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(0, -0.5 * s, 0, 0.5 * s)
  }
  if (char.toLowerCase() === 'j') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(0.5 * s, 0, 0.5 * s, -0.5 * s)
    line(0.5 * s, 0, 0, 0.5 * s)
    line(-0.5 * s, 0.5 * s, 0, 0.5 * s)
    line(-0.5 * s, 0.5 * s, -0.5 * s, 0)
  }
  if (char.toLowerCase() === 'k') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0, 0)
    line(0.5 * s, 0.5 * s, 0, 0)
    line(0.5 * s, -0.5 * s, 0, 0)
  }
  if (char.toLowerCase() === 'l') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, 0.5 * s, -0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === 'm') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0, 0)
    line(0.5 * s, -0.5 * s, 0, 0)
  }
  if (char.toLowerCase() === 'n') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === 'o') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === 'p') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(0, 0, 0.5 * s, -0.5 * s)
    line(0, 0, -0.5 * s, 0)
  }
  if (char.toLowerCase() === 'q') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(0.5 * s, 0.5 * s, 0, 0)
  }
  if (char.toLowerCase() === 'r') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(0, 0, 0.5 * s, -0.5 * s)
    line(0, 0, -0.5 * s, 0)
    line(0, 0, 0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === 's') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(0.5 * s, 0, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, -0.5 * s, -0.5 * s)
  }
  if (char.toLowerCase() === 't') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(0, -0.5 * s, 0, 0.5 * s)
  }
  if (char.toLowerCase() === 'u') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === 'v') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0)
    line(0, 0.5 * s, -0.5 * s, 0)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0)
    line(0, 0.5 * s, 0.5 * s, 0)
  }
  if (char.toLowerCase() === 'w') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(0, 0.5 * s, 0, 0)
  }
  if (char.toLowerCase() === 'x') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, -0.5 * s)
  }
  if (char.toLowerCase() === 'y') {
    line(0, 0, -0.5 * s, -0.5 * s)
    line(0, 0, 0.5 * s, -0.5 * s)
    line(0, 0, 0, 0.5 * s)
  }
  if (char.toLowerCase() === 'z') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === '0') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(0.5 * s, -0.5 * s, -0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === '1') {
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(0, 0.5 * s, 0, -0.5 * s)
    line(-0.5 * s, -0.5 * s, 0, -0.5 * s)
  }
  if (char.toLowerCase() === '2') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(-0.5 * s, 0, -0.5 * s, 0.5 * s)
    line(0.5 * s, 0, 0.5 * s, -0.5 * s)
  }
  if (char.toLowerCase() === '3') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
  }
  if (char.toLowerCase() === '4') {
    line(-0.5 * s, -0.5 * s, -0.5 * s, 0)
    line(0.5 * s, -0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
  }
  if (char.toLowerCase() === '5') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(0.5 * s, 0, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, -0.5 * s, -0.5 * s)
  }
  if (char.toLowerCase() === '6') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(0.5 * s, 0, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0.5 * s, -0.5 * s, -0.5 * s)
  }
  if (char.toLowerCase() === '7') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(0, 0, 0.5 * s, -0.5 * s)
    line(0, 0, 0, 0.5 * s)
  }
  if (char.toLowerCase() === '8') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(-0.5 * s, 0.5 * s, -0.5 * s, -0.5 * s)
    line(0.5 * s, 0.5 * s, 0.5 * s, -0.5 * s)
  }
  if (char.toLowerCase() === '9') {
    line(-0.5 * s, -0.5 * s, 0.5 * s, -0.5 * s)
    line(-0.5 * s, 0.5 * s, 0.5 * s, 0.5 * s)
    line(-0.5 * s, 0, 0.5 * s, 0)
    line(-0.5 * s, 0, -0.5 * s, -0.5 * s)
    line(0.5 * s, 0.5 * s, 0.5 * s, -0.5 * s)
  }
}
